﻿using System;
using System.Windows;
using Vitcs.Workstation.Client.ApiServices;

namespace Vitcs.Workstation.Client
{
    public partial class App : Application
    {
        private static readonly ExceptionApiService _exceptionApiService = new ExceptionApiService();

        static App()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private static async void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                if (e.ExceptionObject is Exception ex)
                    await _exceptionApiService.SaveException(ex);
            }
            catch
            {
            }
        }
    }
}
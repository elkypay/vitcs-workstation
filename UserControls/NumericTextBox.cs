﻿using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace Vitcs.Workstation.Client.UserControls
{
    public class NumericTextBox : TextBox
    {
        public NumericTextBox() => PreviewTextInput += TextBox_PreviewTextInput;

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e) =>
            e.Handled = !Regex.IsMatch(e.Text, "^[0-9]+$");
    }
}
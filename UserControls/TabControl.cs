﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Vitcs.Workstation.Client.UserControls
{
    public class TabControl : System.Windows.Controls.TabControl
    {
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);
            var selectedItem = (TabItem)SelectedItem;
            Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() =>
            {
                selectedItem.Focus();
                selectedItem.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }));
        }
    }
}
﻿using System.Windows;
using System.Windows.Controls;
using Vitcs.Workstation.Client.Models.Step5;

namespace Vitcs.Workstation.Client
{
    public partial class Step5PrintWindow : Window
    {
        public Step5PrintWindow(Step5WaybillData step5WaybillData)
        {
            InitializeComponent();

            DataContext = step5WaybillData;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            new PrintDialog().PrintVisual(this, null);
            Close();
        }
    }
}
﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Vitcs.Workstation.Client.ValidationRules
{
    public class RegexValidationRule : ValidationRule
    {
        private string _pattern;
        private Regex _regex;

        public string Pattern
        {
            get => _pattern;
            set
            {
                _pattern = value;
                _regex = new Regex(_pattern, RegexOptions.IgnoreCase);
            }
        }

        public string ErrorMessage { get; set; }

        public override ValidationResult Validate(object value, CultureInfo ultureInfo)
        {
            string text = (string)value;
            if (string.IsNullOrEmpty(text) || !_regex.Match(text).Success)
                return new ValidationResult(false, ErrorMessage);

            return new ValidationResult(true, null);
        }
    }
}
﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Vitcs.Workstation.Client.ApiServices;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step2;

namespace Vitcs.Workstation.Client
{
    public partial class Step2_2Window : Window, INotifyPropertyChanged
    {
        public int? ActivityId { get; set; }

        public int? InspectorId { get; set; }

        private string _inspectorName;

        public string InspectorName
        {
            get => _inspectorName;
            set
            {
                _inspectorName = $"Hello, {value}";
                OnPropertyChanged(nameof(InspectorName));
            }
        }

        private DateTime _currentTime;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                _currentTime = value;
                OnPropertyChanged(nameof(CurrentTime));
            }
        }

        public string AccessCard { get; set; }

        public string TruckTag { get; set; }

        public string TruckContainerTag { get; set; }

        public string InspectionComment { get; set; }

        private readonly Step2ApiService _apiService = new Step2ApiService();
        private readonly DispatcherTimer _currentTimeUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };

        public Step2_2Window()
        {
            InitializeComponent();

            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");

            DataContext = this;

            _currentTimeUpdateTimer.Start();
            _currentTimeUpdateTimer.Tick += CurrentTimeUpdateTimer_Tick;
        }

        private async void InspectorAuthenticationButton_Click(object sender, RoutedEventArgs e)
        {
            Step2InspectorData inspectorData = await _apiService.PostStep2InspectorCredentials(LoginTextBox.Text, PasswordTextBox.Password);

            if (inspectorData != null)
            {
                InspectorId = inspectorData.Id;
                InspectorName = inspectorData.FullName;
                TruckSearchTabItem.IsSelected = true;
            }
            else
                ShowContactSupervisorDialog("Wrong login or password entered");
        }

        private async void TruckSearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (AccessCardTextBox.HasValidationError()) return;
            if (TruckTagTextBox.HasValidationError()) return;
            if (TruckContainerTagTextBox.HasValidationError()) return;

            ActivityId = await _apiService.GetStep2TruckWaitingForInspection(
                $"AC{AccessCardTextBox.Text}", DriverFullNameTextBox.Text, IqamaNoTextBox.Text, $"TT{TruckTagTextBox.Text}", $"CC{TruckContainerTagTextBox.Text}");

            if (ActivityId != null)
                InspectionTabItem.IsSelected = true;
            else
                ShowContactSupervisorDialog("Wrong truck data entered.");
        }

        private async void InspectionVerdictButton_Click(object sender, RoutedEventArgs e)
        {
            string inspectionStatus;
            string inspectionComment = null;

            if (AccessGrantedRadioButton.IsChecked == true)
            {
                inspectionStatus = "Access Granted";
            }
            else if (AccessGrantedWithPenaltyRadioButton.IsChecked == true)
            {
                inspectionStatus = "Access Granted With Penalty";
                inspectionComment = AccessGrantedWithPenaltyCommentTextBox.Text;
                if (AccessGrantedWithPenaltyCommentTextBox.HasValidationError()) return;
            }
            else
            {
                inspectionStatus = "Access Denied";
                inspectionComment = AccessDeniedCommentTextBox.Text;
                if (AccessDeniedCommentTextBox.HasValidationError()) return;
            }

            await _apiService.PatchStep2InspectionStatus(ActivityId.Value, InspectorId.Value, inspectionStatus, inspectionComment);
            StartOver();
        }

        private void ShowContactSupervisorDialog(string errorMessage)
        {
            ErrorMessageLabel.Content = errorMessage;
            ContactYourSupervisorOverlay.Visibility = Visibility.Visible;
            TabControl.IsEnabled = false;
        }

        private void StartOver()
        {
            new Step2_2Window().Show();
            Close();
        }

        private void StartOverButton_Click(object sender, RoutedEventArgs e) => StartOver();

        private void CurrentTimeUpdateTimer_Tick(object sender, EventArgs e) => CurrentTime = DateTime.Now;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
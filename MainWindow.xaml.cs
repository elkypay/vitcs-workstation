﻿using System.Windows;
using System.Windows.Controls;

namespace Vitcs.Workstation.Client
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LaunchButton_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem selectedItem = WindowComboBox.SelectedItem as ComboBoxItem;

            if (selectedItem == null) return;

            switch (selectedItem.Content)
            {
                case "Step 1":
                    new Step1Window().Show();
                    break;

                case "Step 1 Queue":
                    new Step1QueueWindow().Show();
                    break;

                case "Step 2.1":
                    new Step2_1Window().Show();
                    break;

                case "Step 2.2":
                    new Step2_2Window().Show();
                    break;

                case "Step 3.1":
                    new Step3Window(1).Show();
                    break;

                case "Step 3.2":
                    new Step3Window(2).Show();
                    break;

                case "Step 3.3":
                    new Step3Window(3).Show();
                    break;

                case "Step 3.4":
                    new Step3Window(4).Show();
                    break;

                case "Step 3.5":
                    new Step3Window(5).Show();
                    break;

                case "Step 3.6":
                    new Step3Window(6).Show();
                    break;

                case "Step 5":
                    new Step5Window().Show();
                    break;
            }

            Close();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Vitcs.Workstation.Client.ApiServices;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step5;

namespace Vitcs.Workstation.Client
{
    public partial class Step5Window : Window, INotifyPropertyChanged
    {
        public int? ActivityId { get; set; }

        public IEnumerable<string> Countries { get; set; }

        private DateTime _currentTime;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                _currentTime = value;
                OnPropertyChanged(nameof(CurrentTime));
            }
        }

        private string _driverFullName;

        public string DriverFullName
        {
            get => _driverFullName;
            set
            {
                _driverFullName = string.Format(Properties.Resources.HelloDriver, value);
                OnPropertyChanged(nameof(DriverFullName));
            }
        }

        private bool _isManuallyEnteringPass;

        public string Pass { get; set; }

        private readonly Step5ApiService _apiService = new Step5ApiService();
        private readonly DispatcherTimer _currentTimeUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };

        public Step5Window()
        {
            InitializeComponent();

            DataContext = this;

            _currentTimeUpdateTimer.Start();
            _currentTimeUpdateTimer.Tick += CurrentTimeUpdateTimer_Tick;
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Countries = await _apiService.GetCountries();
            CountriesComboBox.ItemsSource = Countries;
        }

        private void ChangeCulture(string cultureCode)
        {
            CultureInfo cultureInfo = new CultureInfo(cultureCode);
            if (!Thread.CurrentThread.CurrentUICulture.Equals(cultureInfo))
            {
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Step5Window step5Window = new Step5Window();
                step5Window.PassTabItem.IsSelected = true;
                step5Window.Show();
                Close();
            }
            else
                PassTabItem.IsSelected = true;
        }

        private void ArabicLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ar");

        private void EnglishLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("en");

        private void UrduLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ur");

        private void HindiLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("hi");

        private void PassCountdown_Elapsed(object sender, EventArgs e)
        {
            UpdateEnteringUiForPass();
        }

        private void UpdateEnteringUiForPass()
        {
            if (_isManuallyEnteringPass)
                SecurityQuestionsTabItem.IsSelected = true;
            else
            {
                PassTipLabel.Content = Properties.Resources.ManuallyEnterYourPass;
                PassCountdown.Start();

                _isManuallyEnteringPass = true;
            }
        }

        private async void PassValidateButton_Click(object sender, RoutedEventArgs e)
        {
            if (PassTextBox.HasValidationError()) return;
            await CheckPass(PassTextBox.Text);
        }

        private async Task CheckPass(string pass)
        {
            PassCountdown.Stop();

            Step5Activity step5Activity = await _apiService.PostStep5Activity(pass, _isManuallyEnteringPass);
            ActivityId = step5Activity.Id;
            DriverFullName = step5Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongPassErrorMessage);
            else
                WaybillTabItem.IsSelected = true;
        }

        private async void SecurityQuestionsValidateButton_Click(object sender, RoutedEventArgs e)
        {
            Step5Activity step5Activity = await _apiService.PostStep5ActivityUsingSecurityQuestions(
                IqamaNoTextBox.Text, LicenseNoTextBox.Text, CountriesComboBox.Text);
            ActivityId = step5Activity.Id;
            DriverFullName = step5Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongSecurityQuestionAnswersErrorMessage);
            else
                WaybillTabItem.IsSelected = true;
        }

        private async void WaybillTabItem_Selected(object sender, RoutedEventArgs e)
        {
            Step5WaybillData step5WaybillData = await _apiService.GetStep5WaybillData(ActivityId.Value);
            new Step5PrintWindow(step5WaybillData).Show();
            StartOver();
        }

        private void ShowContactSupervisorDialog(string errorMessage)
        {
            ErrorMessageLabel.Content = errorMessage;
            ContactYourSupervisorOverlay.Visibility = Visibility.Visible;
            TabControl.IsEnabled = false;
        }

        private void StartOver()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            new Step5Window().Show();
            Close();
        }

        private void StartOverButton_Click(object sender, RoutedEventArgs e) =>
            StartOver();

        private void CurrentTimeUpdateTimer_Tick(object sender, EventArgs e) =>
            CurrentTime = DateTime.Now;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
﻿using System.Windows;
using System.Windows.Controls;

namespace Vitcs.Workstation.Client.AttachedProperties
{
    public static class Password
    {
        public static bool GetPlaceholder(DependencyObject obj) => (bool)obj.GetValue(PlaceholderProperty);

        public static void SetPlaceholder(DependencyObject obj, string value) => obj.SetValue(PlaceholderProperty, value);

        public static readonly DependencyProperty PlaceholderProperty =
            DependencyProperty.RegisterAttached("Placeholder", typeof(string),
                typeof(Password), new UIPropertyMetadata(PlaceholderChanged));

        private static void PlaceholderChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is PasswordBox passwordBox)) return;

            passwordBox.PasswordChanged -= PasswordChanged;
            passwordBox.PasswordChanged += PasswordChanged;

            void PasswordChanged(object sender, RoutedEventArgs args)
            {
                PasswordBox pb = sender as PasswordBox;
                pb.SetValue(HasPasswordProperty, pb.Password.Length > 0);
            }
        }

        public static bool GetHasPassword(DependencyObject obj) => (bool)obj.GetValue(HasPasswordProperty);

        public static void SetHasPassword(DependencyObject obj, bool value) => obj.SetValue(HasPasswordProperty, value);

        private static readonly DependencyProperty HasPasswordProperty =
            DependencyProperty.RegisterAttached("HasPassword",
                typeof(bool), typeof(Password),
                new FrameworkPropertyMetadata(false));
    }
}
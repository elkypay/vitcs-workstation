﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;
using Vitcs.Workstation.Client.ApiServices;
using Vitcs.Workstation.Client.Models.Step1Queue;

namespace Vitcs.Workstation.Client
{
    public partial class Step1QueueWindow : Window, INotifyPropertyChanged
    {
        private DateTime _currentTime;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                _currentTime = value;
                OnPropertyChanged(nameof(CurrentTime));
            }
        }

        private IEnumerable<int> _pendingQueueNumbers;

        public IEnumerable<int> PendingQueueNumbers
        {
            get => _pendingQueueNumbers;
            set
            {
                _pendingQueueNumbers = value;
                OnPropertyChanged(nameof(PendingQueueNumbers));
            }
        }

        private IEnumerable<int> _readyQueueNumbers;

        public IEnumerable<int> ReadyQueueNumbers
        {
            get => _readyQueueNumbers;
            set
            {
                _readyQueueNumbers = value;
                OnPropertyChanged(nameof(ReadyQueueNumbers));
            }
        }

        private readonly Step1QueueApiService _apiService = new Step1QueueApiService();

        private readonly DispatcherTimer _currentTimeUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };

        private readonly DispatcherTimer _queueDataUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };

        public Step1QueueWindow()
        {
            InitializeComponent();

            DataContext = this;

            _currentTimeUpdateTimer.Start();
            _currentTimeUpdateTimer.Tick += CurrentTimeUpdateTimer_Tick;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            QueueDataUpdateTimer_Tick(null, null);

            _queueDataUpdateTimer.Start();
            _queueDataUpdateTimer.Tick += QueueDataUpdateTimer_Tick;
        }

        private async void QueueDataUpdateTimer_Tick(object sender, EventArgs e)
        {
            Step1QueueData step1QueueData = await _apiService.GetStep1QueueData();
            PendingQueueNumbers = step1QueueData.PendingQueueNumbers;
            ReadyQueueNumbers = step1QueueData.ReadyQueueNumbers;
        }

        private void CurrentTimeUpdateTimer_Tick(object sender, EventArgs e) =>
            CurrentTime = DateTime.Now;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
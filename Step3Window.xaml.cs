﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Vitcs.Workstation.Client.ApiServices;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step3;

namespace Vitcs.Workstation.Client
{
    public partial class Step3Window : Window, INotifyPropertyChanged
    {
        public int? ActivityId { get; set; }

        public IEnumerable<string> Countries { get; set; }

        private DateTime _currentTime;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                _currentTime = value;
                OnPropertyChanged(nameof(CurrentTime));
            }
        }

        private string _driverFullName;

        public string DriverFullName
        {
            get => _driverFullName;
            set
            {
                _driverFullName = string.Format(Properties.Resources.HelloDriver, value);
                OnPropertyChanged(nameof(DriverFullName));
            }
        }

        private int _quantity;

        public int Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                OnPropertyChanged(nameof(Quantity));
            }
        }

        private readonly int _stationId;

        public string Pass { get; set; }

        private readonly Step3ApiService _apiService = new Step3ApiService();
        private readonly DispatcherTimer _currentTimeUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };

        private readonly DispatcherTimer _skidDataUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };

        private static readonly Duration AnimationDuration = new Duration(TimeSpan.FromSeconds(0.5));
        private static readonly Storyboard _quantityStoryboard = new Storyboard();
        private static readonly DoubleAnimation _quantityAnimation = new DoubleAnimation { Duration = AnimationDuration };
        private static readonly Storyboard _temperatureStoryboard = new Storyboard();
        private static readonly DoubleAnimation _temperatureAnimation = new DoubleAnimation { Duration = AnimationDuration };
        private static readonly Storyboard _densityStoryboard = new Storyboard();
        private static readonly DoubleAnimation _densityAnimation = new DoubleAnimation { Duration = AnimationDuration };

        private bool _isManuallyEnteringPass;

        public Step3Window(int stationId)
        {
            InitializeComponent();

            _stationId = stationId;

            DataContext = this;

            _currentTimeUpdateTimer.Start();
            _currentTimeUpdateTimer.Tick += CurrentTimeUpdateTimer_Tick;

            PropertyPath anglePropertyPath = new PropertyPath("(0).(1)", new[] { RenderTransformProperty, RotateTransform.AngleProperty });

            Storyboard.SetTarget(_quantityStoryboard, CylinderRectangle);
            Storyboard.SetTargetProperty(_quantityAnimation, new PropertyPath(HeightProperty));
            _quantityStoryboard.Children.Add(_quantityAnimation);

            InitAnimation(_temperatureStoryboard, _temperatureAnimation, TemperatureArrowIcon);
            InitAnimation(_densityStoryboard, _densityAnimation, DensityArrowIcon);

            void InitAnimation(Storyboard storyboard, DoubleAnimation animation, Path path)
            {
                Storyboard.SetTarget(storyboard, path);
                Storyboard.SetTargetProperty(animation, anglePropertyPath);
                storyboard.Children.Add(animation);
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Countries = await _apiService.GetCountries();
            CountriesComboBox.ItemsSource = Countries;
        }

        private void ChangeCulture(string cultureCode)
        {
            CultureInfo cultureInfo = new CultureInfo(cultureCode);
            if (!Thread.CurrentThread.CurrentUICulture.Equals(cultureInfo))
            {
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Step3Window step3Window = new Step3Window(_stationId);
                step3Window.PassTabItem.IsSelected = true;
                step3Window.Show();
                Close();
            }
            else
                PassTabItem.IsSelected = true;
        }

        private void ArabicLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ar");

        private void EnglishLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("en");

        private void UrduLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ur");

        private void HindiLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("hi");

        private void PassCountdown_Elapsed(object sender, EventArgs e) => UpdateEnteringUiForPass();

        private void UpdateEnteringUiForPass()
        {
            if (_isManuallyEnteringPass)
                SecurityQuestionsTabItem.IsSelected = true;
            else
            {
                PassTipLabel.Content = Properties.Resources.ManuallyEnterYourPass;
                PassCountdown.Start();

                _isManuallyEnteringPass = true;
            }
        }

        private async void PassValidateButton_Click(object sender, RoutedEventArgs e)
        {
            if (PassTextBox.HasValidationError()) return;
            await CheckPass(PassTextBox.Text);
        }

        private async Task CheckPass(string pass)
        {
            PassCountdown.Stop();

            Step3Activity step3Activity = await _apiService.PostStep3Activity(_stationId, pass, _isManuallyEnteringPass);
            ActivityId = step3Activity.Id;
            DriverFullName = step3Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongPassErrorMessage);
            else
                SkidTabItem.IsSelected = true;
        }

        private async void SecurityQuestionsValidateButton_Click(object sender, RoutedEventArgs e)
        {
            Step3Activity step3Activity = await _apiService.PostStep3ActivityUsingSecurityQuestions(
                _stationId, IqamaNoTextBox.Text, LicenseNoTextBox.Text, CountriesComboBox.Text);
            ActivityId = step3Activity.Id;
            DriverFullName = step3Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongSecurityQuestionAnswersErrorMessage);
            else
                SkidTabItem.IsSelected = true;
        }

        private void SkidTabItem_Selected(object sender, RoutedEventArgs e)
        {
            SkidDataUpdateTimer_Tick(null, null);

            _skidDataUpdateTimer.Start();
            _skidDataUpdateTimer.Tick += SkidDataUpdateTimer_Tick;
        }

        private async void SkidDataUpdateTimer_Tick(object sender, EventArgs e)
        {
            Step3ActivityOilData step3ActivityOilData = await _apiService.GetStep3ActivityOilData(ActivityId.Value);

            if (step3ActivityOilData.StationStatus == "Idle")
            {
                _skidDataUpdateTimer.Stop();
                _skidDataUpdateTimer.Tick -= SkidDataUpdateTimer_Tick;
                StartOver();
                return;
            }

            Quantity = step3ActivityOilData.Quantity;
            _quantityAnimation.To = step3ActivityOilData.Quantity / 100.0 * 537.5;
            _quantityStoryboard.Begin();

            RunAnimation(_temperatureStoryboard, _temperatureAnimation, step3ActivityOilData.Temperature, 100.0);
            RunAnimation(_densityStoryboard, _densityAnimation, step3ActivityOilData.Density, 100.0);

            void RunAnimation(Storyboard storyboard, DoubleAnimation animation, double newValue, double maximum)
            {
                const double angleOffset = 135.0;

                animation.To = newValue / maximum * 180.0 - angleOffset;
                storyboard.Begin();
            }
        }

        private void ShowContactSupervisorDialog(string errorMessage)
        {
            ErrorMessageLabel.Content = errorMessage;
            ContactYourSupervisorOverlay.Visibility = Visibility.Visible;
            TabControl.IsEnabled = false;
        }

        private void StartOver()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            new Step3Window(_stationId).Show();
            Close();
        }

        private void StartOverButton_Click(object sender, RoutedEventArgs e) =>
            StartOver();

        private void CurrentTimeUpdateTimer_Tick(object sender, EventArgs e) =>
            CurrentTime = DateTime.Now;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
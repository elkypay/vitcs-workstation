﻿using System;

namespace Vitcs.Workstation.Client.Models.Step5
{
    public class Step5WaybillData
    {
        public int WaybillId { get; set; }

        public string WaybillBarcode { get; set; }

        public string WaybillBarcodeImageRef => $"https://chart.googleapis.com/chart?cht=qr&chs=100x100&chl={WaybillBarcode}";

        public string ContractorName { get; set; }

        public string DriverFullName { get; set; }

        public DateTime Step1CheckIn { get; set; }

        public DateTime Step3CheckOut { get; set; }

        public int Step3Quantity { get; set; }
    }
}
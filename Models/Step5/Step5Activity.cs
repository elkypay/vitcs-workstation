﻿namespace Vitcs.Workstation.Client.Models.Step5
{
    public class Step5Activity
    {
        public int? Id { get; set; }

        public string DriverFullName { get; set; }
    }
}
﻿namespace Vitcs.Workstation.Client.Models.Step3
{
    public class Step3ActivityOilData
    {
        public int Quantity { get; set; }

        public int Temperature { get; set; }

        public int Density { get; set; }

        public string StationStatus { get; set; }
    }
}
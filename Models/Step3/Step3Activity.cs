﻿namespace Vitcs.Workstation.Client.Models.Step3
{
    public class Step3Activity
    {
        public int? Id { get; set; }

        public string DriverFullName { get; set; }
    }
}
﻿using System;

namespace Vitcs.Workstation.Client.Models.Step1
{
    public class Step1PrintedPassData
    {
        public string Barcode { get; set; }

        public string DriverFullName { get; set; }

        public string Waybill { get; set; }

        public int QueueNumber { get; set; }

        public DateTime CheckOut { get; set; }
    }
}
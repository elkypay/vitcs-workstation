﻿namespace Vitcs.Workstation.Client.Models.Step1
{
    public class Step1Activity
    {
        public int? Id { get; set; }

        public string DriverFullName { get; set; }
    }
}
﻿namespace Vitcs.Workstation.Client.Models.Step1
{
    public class AuthenticationMode
    {
        public bool FingerprintMode { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace Vitcs.Workstation.Client.Models.Step1Queue
{
    public class Step1QueueData
    {
        public IEnumerable<int> PendingQueueNumbers { get; set; }

        public IEnumerable<int> ReadyQueueNumbers { get; set; }
    }
}
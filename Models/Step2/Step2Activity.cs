﻿namespace Vitcs.Workstation.Client.Models.Step2
{
    public class Step2Activity
    {
        public int? Id { get; set; }

        public string DriverFullName { get; set; }
    }
}
﻿namespace Vitcs.Workstation.Client.Models.Step2
{
    public class Step2InspectorData
    {
        public int Id { get; set; }

        public string FullName { get; set; }
    }
}
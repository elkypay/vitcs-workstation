﻿using System;

namespace Vitcs.Workstation.Client.Models.Step2
{
    public class Step2PrintedPassData
    {
        public string Barcode { get; set; }

        public string DriverFullName { get; set; }

        public string TruckTag { get; set; }

        public string TruckContainerTag { get; set; }

        public string Waybill { get; set; }

        public DateTime CheckOut { get; set; }

        public string InspectionStatus { get; set; }

        public string InspectionComment { get; set; }
    }
}
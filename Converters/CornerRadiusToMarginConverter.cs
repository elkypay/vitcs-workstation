﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vitcs.Workstation.Client.Converters
{
    public class CornerRadiusToMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            CornerRadius cornerRadius = (CornerRadius)value;
            return new Thickness(cornerRadius.BottomLeft, 0.0, cornerRadius.BottomRight, 0.0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();
    }
}
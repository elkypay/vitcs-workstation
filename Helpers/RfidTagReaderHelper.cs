﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using nsAlienRFID2;

namespace Vitcs.Workstation.Client.Helpers
{
    public static class RfidTagReaderHelper
    {
        private static readonly clsReader _reader = new clsReader();
        private static readonly Timer _rfidTagReaderIimer = new Timer { Interval = TimeSpan.FromSeconds(2).TotalMilliseconds };

        public static event EventHandler DataReceived;

        public async static Task<bool> Connect()
        {
            _reader.InitOnNetwork("192.168.1.101", 23);

            await Task.Run(() => _reader.Connect());

            if (!_reader.IsConnected)
                return false;

            if (!_reader.Login("alien", "password"))
                _reader.Disconnect();

            _reader.AutoMode = "On";

            _rfidTagReaderIimer.Start();
            _rfidTagReaderIimer.Elapsed -= RfidTagReaderIimer_Elapsed;
            _rfidTagReaderIimer.Elapsed += RfidTagReaderIimer_Elapsed;

            return false;
        }

        private static void RfidTagReaderIimer_Elapsed(object sender, ElapsedEventArgs e) =>
            DataReceived?.Invoke(null, EventArgs.Empty);

        public static string GetTagId(string tagIdRegexPattern)
        {
            _reader.TagListFormat = "XML";

            if (AlienUtils.ParseTagList(_reader.TagList, out TagInfo[] tags) == 0)
                return null;

            Match match = tags
                .Select(tag => Regex.Match(tag.TagID, tagIdRegexPattern))
                .FirstOrDefault(m => m.Success);

            if (match == null)
                return null;

            return match.Groups.Cast<Group>().Last().Value;
        }

        public static void Disconnect()
        {
            _rfidTagReaderIimer.Stop();
            _rfidTagReaderIimer.Elapsed -= RfidTagReaderIimer_Elapsed;
            _reader.Disconnect();
        }
    }
}
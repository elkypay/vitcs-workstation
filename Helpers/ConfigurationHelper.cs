﻿using System;
using System.Configuration;
using System.Windows;

namespace Vitcs.Workstation.Client.Helpers
{
    public static class ConfigurationHelper
    {
        public static string ApiBaseUrl => ConfigurationManager.AppSettings[nameof(ApiBaseUrl)];

        public static string DateTimeFormat => ConfigurationManager.AppSettings[nameof(DateTimeFormat)];

        public static Duration CountdownDuration => new Duration(TimeSpan.Parse(ConfigurationManager.AppSettings[nameof(CountdownDuration)]));
    }
}
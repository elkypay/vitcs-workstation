﻿using System.Collections.Generic;
using System.Linq;
using Custom.PowerToolApi;
using Vitcs.Workstation.Client.Exceptions;
using Vitcs.Workstation.Client.Models.Step1;
using Vitcs.Workstation.Client.Models.Step2;

namespace Vitcs.Workstation.Client.Helpers
{
    public static class PassPrinterHelper
    {
        public static void PrintStep1Pass(Step1PrintedPassData step1PrintedPassData)
        {
            using (CustomXmlPrinter xmlPrinter = new CustomXmlPrinter())
            {
                xmlPrinter.SetXmlFileName(@"PassPrintingTemplates\Step1\PassPrinterStep1.xml");
                xmlPrinter.GetXmlObjectListFromParamFile(@"PassPrintingTemplates\Step1\PassPrinterStep1.param.xml", out CustomDictionaryItem[] items);

                SetXmlVariableValues(items, new Dictionary<string, string>
                {
                    ["PassBarocde.text"] = step1PrintedPassData.Barcode,
                    ["Driver_Value.text"] = step1PrintedPassData.DriverFullName,
                    ["Waybill_Value.text"] = step1PrintedPassData.Waybill,
                    ["QueueNb_Value.text"] = step1PrintedPassData.QueueNumber.ToString(),
                    ["CheckoutTime_Value.text"] = step1PrintedPassData.CheckOut.ToString(ConfigurationHelper.DateTimeFormat)
                });

                Print(xmlPrinter, items);
            }
        }

        public static void PrintStep2Pass(Step2PrintedPassData step2PrintedPassData)
        {
            using (CustomXmlPrinter xmlPrinter = new CustomXmlPrinter())
            {
                xmlPrinter.SetXmlFileName(@"PassPrintingTemplates\Step2\PassPrinterStep2.xml");
                xmlPrinter.GetXmlObjectListFromParamFile(@"PassPrintingTemplates\Step2\PassPrinterStep2.param.xml", out CustomDictionaryItem[] items);

                SetXmlVariableValues(items, new Dictionary<string, string>
                {
                    ["PassBarocde.text"] = step2PrintedPassData.Barcode,
                    ["Driver_Value.text"] = step2PrintedPassData.DriverFullName,
                    ["Windshield_Value.text"] = step2PrintedPassData.TruckTag,
                    ["Container_Value.text"] = step2PrintedPassData.TruckContainerTag,
                    ["Waybill_Value.text"] = step2PrintedPassData.Waybill,
                    ["CheckoutTime_Value.text"] = step2PrintedPassData.CheckOut.ToString(ConfigurationHelper.DateTimeFormat),
                    ["InspectionComment_Value.text"] = step2PrintedPassData.InspectionComment,
                    ["AccessType_Value.text"] = step2PrintedPassData.InspectionStatus
                });

                Print(xmlPrinter, items);
            }
        }

        private static void SetXmlVariableValues(CustomDictionaryItem[] items, Dictionary<string, string> dictionary)
        {
            foreach (KeyValuePair<string, string> entry in dictionary)
                SetXmlVariableValue(entry.Key, entry.Value);

            void SetXmlVariableValue(string key, string value) =>
                items.First(item => item.Tag == key).Value = value;
        }

        private static void Print(CustomXmlPrinter xmlPrinter, CustomDictionaryItem[] items)
        {
            foreach (CustomDictionaryItem item in items)
                xmlPrinter.SetXmlObjectTag(item.Tag, item.Value);

            xmlPrinter.SetXmlObjectTag("Canvas1.antialiasText", "1");
            xmlPrinter.SetXmlObjectTag("Canvas1.antialiasShape", "1");

            // Change method body with commented code to see preview
            //xmlPrinter.PreviewXml(out string tmpFileName);
            //System.Diagnostics.Process.Start(tmpFileName);
            CustomXmlPrintResult result = xmlPrinter.PrintXml(false, false);
            if (result != CustomXmlPrintResult.CUSTOM_XMLPRINT_SUCCESS)
                throw result.ToException();
        }

        private static PassPrintingException ToException(this CustomXmlPrintResult result) => new PassPrintingException(result.ToString());
    }
}
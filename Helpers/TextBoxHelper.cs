﻿using System.Windows.Controls;

namespace Vitcs.Workstation.Client.Helpers
{
    public static class TextBoxHelper
    {
        public static bool HasValidationError(this TextBox textBox)
        {
            textBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            return Validation.GetHasError(textBox);
        }
    }
}
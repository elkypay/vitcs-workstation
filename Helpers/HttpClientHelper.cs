﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Vitcs.Workstation.Client.Helpers
{
    public static class HttpClientHelper
    {
        public static async Task<T> GetJsonModel<T>(this HttpClient httpClient, string requestUri)
        {
            HttpResponseMessage httpResponseMessage = await httpClient.GetAsync(requestUri);
            string json = await httpResponseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static async Task<T> PostJsonModel<T>(this HttpClient httpClient, string requestUri, object parameters)
        {
            StringContent stringContent = new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await httpClient.PostAsync(requestUri, stringContent);
            string json = await httpResponseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static Task<HttpResponseMessage> PatchAsync(this HttpClient httpClient, string requestUri, HttpContent content) =>
            httpClient.SendAsync(new HttpRequestMessage(new HttpMethod("PATCH"), requestUri) { Content = content });

        public static async Task<T> PatchJsonModel<T>(this HttpClient httpClient, string requestUri, object parameters)
        {
            StringContent stringContent = new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await httpClient.PatchAsync(requestUri, stringContent);
            string json = await httpResponseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
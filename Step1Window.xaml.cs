﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Vitcs.Workstation.Client.ApiServices;
using Vitcs.Workstation.Client.Exceptions;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models;
using Vitcs.Workstation.Client.Models.Step1;

namespace Vitcs.Workstation.Client
{
    public partial class Step1Window : Window, INotifyPropertyChanged
    {
        public bool FingerprintMode { get; set; }

        public int? ActivityId { get; set; }

        public IEnumerable<string> Countries { get; set; }

        private DateTime _currentTime;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                _currentTime = value;
                OnPropertyChanged(nameof(CurrentTime));
            }
        }

        private string _driverFullName;

        public string DriverFullName
        {
            get => _driverFullName;
            set
            {
                _driverFullName = string.Format(Properties.Resources.HelloDriver, value);
                OnPropertyChanged(nameof(DriverFullName));
            }
        }

        public string AccessCard { get; set; }

        public string Waybill { get; set; }

        private static class WaybillInputMethod
        {
            public const string RfidTag = nameof(RfidTag);

            public const string Barcode = nameof(Barcode);

            public const string Manual = nameof(Manual);
        }

        private readonly Step1ApiService _apiService = new Step1ApiService();
        private readonly DispatcherTimer _currentTimeUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };

        private bool _isManuallyEnteringAccessCard;

        private string _waybillInputMethod = WaybillInputMethod.RfidTag;

        public Step1Window()
        {
            InitializeComponent();

            DataContext = this;

            _currentTimeUpdateTimer.Start();
            _currentTimeUpdateTimer.Tick += CurrentTimeUpdateTimer_Tick;
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AuthenticationMode authenticationMode = await _apiService.GetAuthenticationMode();
            FingerprintMode = authenticationMode.FingerprintMode;

            if (!FingerprintMode)
                TabControl.Items.Remove(FingerprintTabItem);

            Countries = await _apiService.GetCountries();
            CountriesComboBox.ItemsSource = Countries;
        }

        private void ChangeCulture(string cultureCode)
        {
            CultureInfo cultureInfo = new CultureInfo(cultureCode);
            if (!Thread.CurrentThread.CurrentUICulture.Equals(cultureInfo))
            {
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Step1Window step1Window = new Step1Window();
                step1Window.AccessCardTabItem.IsSelected = true;
                step1Window.Show();
                Close();
            }
            else
                AccessCardTabItem.IsSelected = true;
        }

        private void ArabicLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ar");

        private void EnglishLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("en");

        private void UrduLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ur");

        private void HindiLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("hi");

        private async void AccessCardTabItem_Selected(object sender, RoutedEventArgs e)
        {
            if (!await RfidTagReaderHelper.Connect())
            {
                RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
                RfidTagReaderHelper.Disconnect();
                UpdateEnteringUiForAccessCard();
                return;
            }

            RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
            RfidTagReaderHelper.DataReceived += AccessCardRfidTagReader_DataReceived;
        }

        private async void AccessCardRfidTagReader_DataReceived(object sender, EventArgs e)
        {
            string tagId = RfidTagReaderHelper.GetTagId("^AC[0-9]{4} ([0-9]{4})$");
            if (tagId == null) return;

            RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
            RfidTagReaderHelper.Disconnect();
            await CheckAccessCard(tagId);
        }

        private void AccessCardCountdown_Elapsed(object sender, EventArgs e)
        {
            RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
            RfidTagReaderHelper.Disconnect();

            UpdateEnteringUiForAccessCard();
        }

        private void UpdateEnteringUiForAccessCard()
        {
            if (_isManuallyEnteringAccessCard)
            {
                SecurityQuestionsTabItem.IsSelected = true;
                return;
            }

            AccessCardTipLabel.Content = Properties.Resources.ManuallyEnterYourAccessCard;
            ManuallyEnteringAccessCardContainer.Visibility = Visibility.Visible;
            AccessCardCountdown.Start();
            AccessCardTextBox.Focus();

            _isManuallyEnteringAccessCard = true;
        }

        private async void AccessCardValidateButton_Click(object sender, RoutedEventArgs e)
        {
            if (AccessCardTextBox.HasValidationError()) return;
            await CheckAccessCard(AccessCardTextBox.Text);
        }

        private async Task CheckAccessCard(string accessCardText)
        {
            AccessCardCountdown.Stop();

            accessCardText = $"AC{accessCardText}";

            if (FingerprintMode)
            {
                if (await _apiService.GetAccessCardExistence(accessCardText))
                {
                    FingerprintTabItem.IsSelected = true;
                    return;
                }

                await _apiService.PostStep1Activity(accessCardText, _isManuallyEnteringAccessCard, null);
            }
            else
            {
                Step1Activity step1Activity = await _apiService.PostStep1Activity(accessCardText, _isManuallyEnteringAccessCard, null);
                ActivityId = step1Activity.Id;
                DriverFullName = step1Activity.DriverFullName;
            }

            if (ActivityId == null)
            {
                ShowContactSupervisorDialog(Properties.Resources.WrongAccessCardErrorMessage);
            }
            else
            {
                if (FingerprintMode)
                    FingerprintTabItem.IsSelected = true;
                else
                    WaybillTabItem.IsSelected = true;
            }
        }

        private void FingerprintCountdown_Elapsed(object sender, EventArgs e) => SecurityQuestionsTabItem.IsSelected = true;

        private async void FingerprintValidateButton_Click(object sender, RoutedEventArgs e)
        {
            FingerprintCountdown.Stop();

            string accessCardText = !string.IsNullOrEmpty(AccessCardTextBox.Text) ? $"AC{AccessCardTextBox.Text}" : null;
            Step1Activity step1Activity = await _apiService.PostStep1Activity(accessCardText, true, FingerprintTextBox.Text);
            ActivityId = step1Activity.Id;
            DriverFullName = step1Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongFingerprintErrorMessage);
            else
                WaybillTabItem.IsSelected = true;
        }

        private async void SecurityQuestionsValidateButton_Click(object sender, RoutedEventArgs e)
        {
            Step1Activity step1Activity = await _apiService.PostStep1ActivityUsingSecurityQuestions(IqamaNoTextBox.Text, LicenseNoTextBox.Text, CountriesComboBox.Text);
            ActivityId = step1Activity.Id;
            DriverFullName = step1Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongSecurityQuestionAnswersErrorMessage);
            else
                WaybillTabItem.IsSelected = true;
        }

        private async void WaybillTabItem_Selected(object sender, RoutedEventArgs e)
        {
            if (!await RfidTagReaderHelper.Connect())
            {
                RfidTagReaderHelper.DataReceived -= WaybillRfidTagReader_DataReceived;
                RfidTagReaderHelper.Disconnect();
                UpdateEnteringUiForWaybill();
                return;
            }

            RfidTagReaderHelper.DataReceived -= WaybillRfidTagReader_DataReceived;
            RfidTagReaderHelper.DataReceived += WaybillRfidTagReader_DataReceived;
        }

        private async void WaybillRfidTagReader_DataReceived(object sender, EventArgs e)
        {
            string tagId = RfidTagReaderHelper.GetTagId("^[0-9]{10}$");
            if (tagId == null) return;

            RfidTagReaderHelper.DataReceived -= WaybillRfidTagReader_DataReceived;
            RfidTagReaderHelper.Disconnect();
            await CheckWaybill(tagId);
        }

        private void WaybillCountdown_Elapsed(object sender, EventArgs e)
        {
            RfidTagReaderHelper.DataReceived -= WaybillRfidTagReader_DataReceived;
            RfidTagReaderHelper.Disconnect();

            UpdateEnteringUiForWaybill();
        }

        private void UpdateEnteringUiForWaybill()
        {
            switch (_waybillInputMethod)
            {
                case WaybillInputMethod.RfidTag:
                    WaybillTipLabel.Content = Properties.Resources.ScanYourWaybillBarcode;
                    ShowManuallyEnteringUiForWaybill();

                    _waybillInputMethod = WaybillInputMethod.Barcode;
                    break;

                case WaybillInputMethod.Barcode:
                    WaybillTipLabel.Content = Properties.Resources.ManuallyEnterYourWaybill;
                    ShowManuallyEnteringUiForWaybill();

                    _waybillInputMethod = WaybillInputMethod.Manual;
                    break;

                case WaybillInputMethod.Manual:
                    ShowContactSupervisorDialog(Properties.Resources.WaybillEnteringTimeoutExpiredErrorMessage);
                    break;
            }

            void ShowManuallyEnteringUiForWaybill()
            {
                ManuallyEnteringWaybillContainer.Visibility = Visibility.Visible;
                WaybillCountdown.Start();
                WaybillTextBox.Focus();
            }
        }

        private async void WaybillValidateButton_Click(object sender, RoutedEventArgs e)
        {
            if (WaybillTextBox.HasValidationError()) return;
            await CheckWaybill(WaybillTextBox.Text);
        }

        public async Task CheckWaybill(string waybill)
        {
            WaybillCountdown.Stop();

            OperationResult result = await _apiService.PatchStep1ActivityWithWaybill(ActivityId.Value, waybill, _waybillInputMethod);

            if (result.IsSuccess)
                PassTabItem.IsSelected = true;
            else
            {
                switch (result.ErrorMessage)
                {
                    case "WRONG_WAYBILL_NUMBER":
                        ShowContactSupervisorDialog(Properties.Resources.WrongWaybillErrorMessage);
                        break;

                    case "EXPIRED_WAYBILL":
                        ShowContactSupervisorDialog(Properties.Resources.ExpiredWaybillErrorMessage);
                        break;
                }
            }
        }

        private void ShowContactSupervisorDialog(string errorMessage)
        {
            ErrorMessageLabel.Content = errorMessage;
            ContactYourSupervisorOverlay.Visibility = Visibility.Visible;
            TabControl.IsEnabled = false;
        }

        private void StartOverButton_Click(object sender, RoutedEventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            new Step1Window().Show();
            Close();
        }

        private async void PassTabItem_Selected(object sender, RoutedEventArgs e)
        {
            Step1PrintedPassData step1PrintedPassData = await _apiService.GetStep1PrintedPassData(ActivityId.Value);
            try
            {
                PassPrinterHelper.PrintStep1Pass(step1PrintedPassData);
                StartOverButton_Click(null, null);
            }
            catch (PassPrintingException ex)
            {
                ShowContactSupervisorDialog(string.Format(Properties.Resources.PrintingPassErrorMessage, ex.Message));
            }
        }

        private void CurrentTimeUpdateTimer_Tick(object sender, EventArgs e) => CurrentTime = DateTime.Now;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
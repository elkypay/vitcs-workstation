﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step2;

namespace Vitcs.Workstation.Client.ApiServices
{
    public class Step2ApiService
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        static Step2ApiService() => _httpClient.BaseAddress = new Uri($"{ConfigurationHelper.ApiBaseUrl}step2/");

        public async Task<Step2Activity> PostStep2Activity(string accessCard, bool isManuallyEnteredAccessCard) =>
            await _httpClient.PostJsonModel<Step2Activity>("activity", new { accessCard, isManuallyEnteredAccessCard });

        public async Task<IEnumerable<string>> GetCountries() =>
            await _httpClient.GetJsonModel<IEnumerable<string>>("countries");

        public async Task<Step2Activity> PostStep2ActivityUsingSecurityQuestions(string iqamaNo, string licenseNo, string country) =>
            await _httpClient.PostJsonModel<Step2Activity>("activity-using-security-questions", new { iqamaNo, licenseNo, country });

        public async Task<bool> PatchStep2ActivityActivityWithTruckTags(int step2ActivityId, string truckTag, string truckContainerTag, bool isManuallyEnteredTruckTags) =>
            await _httpClient.PatchJsonModel<bool>("activity-truck-tags", new { step2ActivityId, truckTag, truckContainerTag, isManuallyEnteredTruckTags });

        public async Task<string> GetStep2InspectionStatus(int id) =>
            await _httpClient.GetJsonModel<string>($"inspection-status?id={id}");

        public async Task<Step2InspectorData> PostStep2InspectorCredentials(string login, string password) =>
            await _httpClient.PostJsonModel<Step2InspectorData>("inspector-login", new { login, password });

        public async Task<int?> GetStep2TruckWaitingForInspection(string driverTagId, string driverFullName, string driverIqamaNo, string truckTagId, string truckContainerTagId) =>
            await _httpClient.GetJsonModel<int?>($"activity-for-inspection?driverTagId={driverTagId}&driverFullName={driverFullName}&driverIqamaNo={driverIqamaNo}&truckTagId={truckTagId}&truckContainerTagId={truckContainerTagId}");

        public async Task PatchStep2InspectionStatus(int step2ActivityId, int inspectorId, string inspectionStatus, string inspectionComment)
        {
            object parameters = new { step2ActivityId, inspectorId, inspectionStatus, inspectionComment };
            StringContent stringContent = new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponseMessage = await _httpClient.PatchAsync("activity-inspection-status", stringContent);
        }

        public async Task<Step2PrintedPassData> GetStep2PrintedPassData(int id) =>
            await _httpClient.GetJsonModel<Step2PrintedPassData>($"printed-pass-data?id={id}");
    }
}
﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step3;

namespace Vitcs.Workstation.Client.ApiServices
{
    public class Step3ApiService
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        static Step3ApiService() => _httpClient.BaseAddress = new Uri($"{ConfigurationHelper.ApiBaseUrl}step3/");

        public async Task<Step3Activity> PostStep3Activity(int stationId, string pass, bool isManuallyEnteredPass) =>
            await _httpClient.PostJsonModel<Step3Activity>("activity", new { stationId, pass, isManuallyEnteredPass });

        public async Task<Step3Activity> PostStep3ActivityUsingSecurityQuestions(int stationId, string iqamaNo, string licenseNo, string country) =>
            await _httpClient.PostJsonModel<Step3Activity>("activity-using-security-questions", new { stationId, iqamaNo, licenseNo, country });

        public async Task<IEnumerable<string>> GetCountries() =>
            await _httpClient.GetJsonModel<IEnumerable<string>>("countries");

        public async Task<Step3ActivityOilData> GetStep3ActivityOilData(int id) =>
            await _httpClient.GetJsonModel<Step3ActivityOilData>($"oil-data?id={id}");
    }
}
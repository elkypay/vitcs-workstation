﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models;
using Vitcs.Workstation.Client.Models.Step1;

namespace Vitcs.Workstation.Client.ApiServices
{
    public class Step1ApiService
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        static Step1ApiService() => _httpClient.BaseAddress = new Uri($"{ConfigurationHelper.ApiBaseUrl}step1/");

        public async Task<AuthenticationMode> GetAuthenticationMode() =>
            await _httpClient.GetJsonModel<AuthenticationMode>("authentication-mode");

        public async Task<bool> GetAccessCardExistence(string accessCard) =>
            await _httpClient.GetJsonModel<bool>($"access-card-existence?accessCard={accessCard}");

        public async Task<Step1Activity> PostStep1Activity(string accessCard, bool isManuallyEnteredAccessCard, string fingerprintId) =>
            await _httpClient.PostJsonModel<Step1Activity>("activity", new { accessCard, isManuallyEnteredAccessCard, fingerprintId });

        public async Task<IEnumerable<string>> GetCountries() =>
            await _httpClient.GetJsonModel<IEnumerable<string>>("countries");

        public async Task<Step1Activity> PostStep1ActivityUsingSecurityQuestions(string iqamaNo, string licenseNo, string country) =>
            await _httpClient.PostJsonModel<Step1Activity>("activity-using-security-questions", new { iqamaNo, licenseNo, country });

        public async Task<OperationResult> PatchStep1ActivityWithWaybill(int step1ActivityId, string waybill, string waybillInputMethod) =>
            await _httpClient.PatchJsonModel<OperationResult>("activity-waybill", new { step1ActivityId, waybill, waybillInputMethod });

        public async Task<Step1PrintedPassData> GetStep1PrintedPassData(int id) =>
            await _httpClient.GetJsonModel<Step1PrintedPassData>($"printed-pass-data?id={id}");
    }
}
﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step1Queue;

namespace Vitcs.Workstation.Client.ApiServices
{
    public class Step1QueueApiService
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        static Step1QueueApiService() => _httpClient.BaseAddress = new Uri($"{ConfigurationHelper.ApiBaseUrl}step1-queue/");

        public async Task<Step1QueueData> GetStep1QueueData() =>
            await _httpClient.GetJsonModel<Step1QueueData>("queue-data");
    }
}
﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step5;

namespace Vitcs.Workstation.Client.ApiServices
{
    public class Step5ApiService
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        static Step5ApiService() => _httpClient.BaseAddress = new Uri($"{ConfigurationHelper.ApiBaseUrl}step5/");

        public async Task<Step5Activity> PostStep5Activity(string pass, bool isManuallyEnteredPass) =>
            await _httpClient.PostJsonModel<Step5Activity>("activity", new { pass, isManuallyEnteredPass });

        public async Task<Step5Activity> PostStep5ActivityUsingSecurityQuestions(string iqamaNo, string licenseNo, string country) =>
            await _httpClient.PostJsonModel<Step5Activity>("activity-using-security-questions", new { iqamaNo, licenseNo, country });

        public async Task<IEnumerable<string>> GetCountries() =>
            await _httpClient.GetJsonModel<IEnumerable<string>>("countries");

        public async Task<Step5WaybillData> GetStep5WaybillData(int id) =>
            await _httpClient.GetJsonModel<Step5WaybillData>($"waybill-data?id={id}");
    }
}
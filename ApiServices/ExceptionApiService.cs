﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Vitcs.Workstation.Client.Helpers;

namespace Vitcs.Workstation.Client.ApiServices
{
    public class ExceptionApiService
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        static ExceptionApiService() => _httpClient.BaseAddress = new Uri(ConfigurationHelper.ApiBaseUrl);

        public async Task SaveException(Exception exception)
        {
            StringContent stringContent = new StringContent(JsonConvert.SerializeObject(exception), Encoding.UTF8, "application/json");
            await _httpClient.PostAsync("error/", stringContent);
        }
    }
}
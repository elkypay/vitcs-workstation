﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Vitcs.Workstation.Client.ApiServices;
using Vitcs.Workstation.Client.Exceptions;
using Vitcs.Workstation.Client.Helpers;
using Vitcs.Workstation.Client.Models.Step2;

namespace Vitcs.Workstation.Client
{
    public partial class Step2_1Window : Window, INotifyPropertyChanged
    {
        public int? ActivityId { get; set; }

        public IEnumerable<string> Countries { get; set; }

        private DateTime _currentTime;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                _currentTime = value;
                OnPropertyChanged(nameof(CurrentTime));
            }
        }

        private string _driverFullName;

        public string DriverFullName
        {
            get => _driverFullName;
            set
            {
                _driverFullName = string.Format(Properties.Resources.HelloDriver, value);
                OnPropertyChanged(nameof(DriverFullName));
            }
        }

        private bool _isManuallyEnteringAccessCard;

        private bool _isManuallyEnteringTruckTags;

        public string AccessCard { get; set; }

        public string TruckTag { get; set; }

        public string TruckContainerTag { get; set; }

        private readonly Step2ApiService _apiService = new Step2ApiService();
        private readonly DispatcherTimer _currentTimeUpdateTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };

        public Step2_1Window()
        {
            InitializeComponent();

            DataContext = this;

            _currentTimeUpdateTimer.Start();
            _currentTimeUpdateTimer.Tick += CurrentTimeUpdateTimer_Tick;
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Countries = await _apiService.GetCountries();
            CountriesComboBox.ItemsSource = Countries;
        }

        private void ChangeCulture(string cultureCode)
        {
            CultureInfo cultureInfo = new CultureInfo(cultureCode);
            if (!Thread.CurrentThread.CurrentUICulture.Equals(cultureInfo))
            {
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Step2_1Window step2Window = new Step2_1Window();
                step2Window.AccessCardTabItem.IsSelected = true;
                step2Window.Show();
                Close();
            }
            else
                AccessCardTabItem.IsSelected = true;
        }

        private void ArabicLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ar");

        private void EnglishLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("en");

        private void UrduLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("ur");

        private void HindiLanguageButton_Click(object sender, RoutedEventArgs e) => ChangeCulture("hi");

        private async void AccessCardTabItem_Selected(object sender, RoutedEventArgs e)
        {
            if (!await RfidTagReaderHelper.Connect())
            {
                RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
                RfidTagReaderHelper.Disconnect();
                UpdateEnteringUiForAccessCard();
                return;
            }

            RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
            RfidTagReaderHelper.DataReceived += AccessCardRfidTagReader_DataReceived;
        }

        private async void AccessCardRfidTagReader_DataReceived(object sender, EventArgs e)
        {
            string tagId = RfidTagReaderHelper.GetTagId("^AC[0-9]{4} ([0-9]{4})$");
            if (tagId == null)
                return;

            RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
            RfidTagReaderHelper.Disconnect();
            await CheckAccessCard(tagId);
        }

        private void AccessCardCountdown_Elapsed(object sender, EventArgs e)
        {
            RfidTagReaderHelper.DataReceived -= AccessCardRfidTagReader_DataReceived;
            RfidTagReaderHelper.Disconnect();

            UpdateEnteringUiForAccessCard();
        }

        private void UpdateEnteringUiForAccessCard()
        {
            if (_isManuallyEnteringAccessCard)
                SecurityQuestionsTabItem.IsSelected = true;
            else
            {
                AccessCardTipLabel.Content = Properties.Resources.ManuallyEnterYourAccessCard;
                ManuallyEnteringAccessCardContainer.Visibility = Visibility.Visible;
                AccessCardCountdown.Start();
                AccessCardTextBox.Focus();

                _isManuallyEnteringAccessCard = true;
            }
        }

        private async void AccessCardValidateButton_Click(object sender, RoutedEventArgs e)
        {
            if (AccessCardTextBox.HasValidationError()) return;
            await CheckAccessCard(AccessCardTextBox.Text);
        }

        private async Task CheckAccessCard(string accessCardText)
        {
            AccessCardCountdown.Stop();

            accessCardText = $"AC{accessCardText}";

            Step2Activity step2Activity = await _apiService.PostStep2Activity(accessCardText, _isManuallyEnteringAccessCard);
            ActivityId = step2Activity.Id;
            DriverFullName = step2Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongAccessCardErrorMessage);
            else
                TruckIdentificationTabItem.IsSelected = true;
        }

        private async void SecurityQuestionsValidateButton_Click(object sender, RoutedEventArgs e)
        {
            Step2Activity step2Activity = await _apiService.PostStep2ActivityUsingSecurityQuestions(IqamaNoTextBox.Text, LicenseNoTextBox.Text, CountriesComboBox.Text);
            ActivityId = step2Activity.Id;
            DriverFullName = step2Activity.DriverFullName;

            if (ActivityId == null)
                ShowContactSupervisorDialog(Properties.Resources.WrongSecurityQuestionAnswersErrorMessage);
            else
                TruckIdentificationTabItem.IsSelected = true;
        }

        private async void TruckIdentificationTabItem_Selected(object sender, RoutedEventArgs e)
        {
            if (!await RfidTagReaderHelper.Connect())
            {
                RfidTagReaderHelper.DataReceived -= TruckIdentificationRfidTagReader_DataReceived;
                RfidTagReaderHelper.Disconnect();
                UpdateUiForTruckIdentification();
                return;
            }

            RfidTagReaderHelper.DataReceived -= TruckIdentificationRfidTagReader_DataReceived;
            RfidTagReaderHelper.DataReceived += TruckIdentificationRfidTagReader_DataReceived;
        }

        private async void TruckIdentificationRfidTagReader_DataReceived(object sender, EventArgs e)
        {
            if (TruckTag == null)
            {
                TruckTag = RfidTagReaderHelper.GetTagId("^TT[0-9]{4} ([0-9]{4})$");
                TruckTagIcon.Visibility = Visibility.Collapsed;
                TruckTagArrowIcon.Visibility = Visibility.Collapsed;
                TruckTagTextBox.Text = TruckTag;
            }

            if (TruckContainerTag == null)
            {
                TruckContainerTag = RfidTagReaderHelper.GetTagId("^CC[0-9]{4} ([0-9]{4})$");
                TruckContainerTagIcon.Visibility = Visibility.Collapsed;
                TruckContainerTagArrowIcon.Visibility = Visibility.Collapsed;
                TruckContainerTagTextBox.Text = TruckContainerTag;
            }

            if (TruckTag != null && TruckContainerTag != null)
            {
                RfidTagReaderHelper.DataReceived -= TruckIdentificationRfidTagReader_DataReceived;
                RfidTagReaderHelper.Disconnect();
                await CheckTruckIdentification(TruckTag, TruckContainerTag);
            }
        }

        private void TruckIdentificationCountdown_Elapsed(object sender, EventArgs e)
        {
            RfidTagReaderHelper.DataReceived -= TruckIdentificationRfidTagReader_DataReceived;
            RfidTagReaderHelper.Disconnect();

            UpdateUiForTruckIdentification();
        }

        private void UpdateUiForTruckIdentification()
        {
            if (_isManuallyEnteringTruckTags)
                ShowContactSupervisorDialog(Properties.Resources.TruckIdentificationTimeoutExpiredErrorMessage);
            else
            {
                TruckIdentificationCardTipLabel.Content = Properties.Resources.ManuallyEnterYourTruckTags;
                ManuallyEnteringTruckIdentificationContainer.Visibility = Visibility.Visible;
                TruckIdentificationCountdown.Start();
                if (string.IsNullOrEmpty(TruckTagTextBox.Text))
                    TruckTagTextBox.Focus();
                else
                    TruckContainerTagTextBox.Focus();

                _isManuallyEnteringTruckTags = true;
            }
        }

        private async void TruckIdentificationValidateButton_Click(object sender, RoutedEventArgs e)
        {
            if (TruckTagTextBox.HasValidationError()) return;
            if (TruckContainerTagTextBox.HasValidationError()) return;
            await CheckTruckIdentification(TruckTagTextBox.Text, TruckContainerTagTextBox.Text);
        }

        public async Task CheckTruckIdentification(string truckTag, string truckContainerTag)
        {
            TruckIdentificationCountdown.Stop();

            truckTag = $"TT{truckTag}";
            truckContainerTag = $"CC{truckContainerTag}";

            bool result = await _apiService.PatchStep2ActivityActivityWithTruckTags(ActivityId.Value, truckTag, truckContainerTag, _isManuallyEnteringTruckTags);

            if (!result)
                ShowContactSupervisorDialog(Properties.Resources.WrongTruckTagsErrorMessage);
            else
            {
                WaitingForInspectionOverlay.Visibility = Visibility.Visible;
                TabControl.IsEnabled = false;

                string status = await _apiService.GetStep2InspectionStatus(ActivityId.Value);

                while (string.IsNullOrEmpty(status))
                {
                    await Task.Delay(TimeSpan.FromSeconds(5.0));
                    status = await _apiService.GetStep2InspectionStatus(ActivityId.Value);
                }

                WaitingForInspectionOverlay.Visibility = Visibility.Collapsed;
                TabControl.IsEnabled = true;
                PassTabItem.IsSelected = true;
            }
        }

        private void ShowContactSupervisorDialog(string errorMessage)
        {
            ErrorMessageLabel.Content = errorMessage;
            ContactYourSupervisorOverlay.Visibility = Visibility.Visible;
            TabControl.IsEnabled = false;
        }

        private void StartOver()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            new Step2_1Window().Show();
            Close();
        }

        private void StartOverButton_Click(object sender, RoutedEventArgs e) => StartOver();

        private async void PassTabItem_Selected(object sender, RoutedEventArgs e)
        {
            Step2PrintedPassData step2PrintedPassData = await _apiService.GetStep2PrintedPassData(ActivityId.Value);
            try
            {
                PassPrinterHelper.PrintStep2Pass(step2PrintedPassData);
                StartOverButton_Click(null, null);
            }
            catch (PassPrintingException ex)
            {
                ShowContactSupervisorDialog(string.Format(Properties.Resources.PrintingPassErrorMessage, ex.Message));
            }
        }

        private void CurrentTimeUpdateTimer_Tick(object sender, EventArgs e) => CurrentTime = DateTime.Now;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
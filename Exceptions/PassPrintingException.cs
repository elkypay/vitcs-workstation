﻿using System;

namespace Vitcs.Workstation.Client.Exceptions
{
    public class PassPrintingException : Exception
    {
        public PassPrintingException() : base()
        {
        }

        public PassPrintingException(string message) : base(message)
        {
        }
    }
}
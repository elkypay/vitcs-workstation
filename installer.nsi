!include "MUI.nsh"
# For taskbar icons
# Needs installation of StdUtils plugin http://nsis.sourceforge.net/StdUtils_plug-in
# To install plugin copy corresponding .nsi plugin file to ${NSISDIR}/Include folder
# and copy corresponding dlls to ${NSISDIR}\Plugins\x86-ansi\ and ${NSISDIR}\Plugins\x86-unicode\ folders
!include 'StdUtils.nsh'

# include a function library that includes a file/directory size reporting command
# for ${GetSize} for EstimatedSize registry entry
!include "FileFunc.nsh"

# To use conditional execution: ${If}, ${Else} and so on
!include LogicLib.nsh

#!define MUI_ICON "VITCS.ico"
!define MUI_PAGE_CUSTOMFUNCTION_PRE GetInstallationDirectoryFromCommandLineArgs
!insertmacro MUI_PAGE_DIRECTORY

!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

Name VITCS

RequestExecutionLevel user

VIProductVersion ${Version}
VIAddVersionKey FileVersion ${Version}
VIAddVersionKey ProductVersion ${Version}

# define installer name
OutFile "VitcsInstaller-${Version}.exe"

# Don't show "Finish" page
AutoCloseWindow true

!define UNINSTALLER_NAME "Uninstaller.exe"
!define START_LINK_DIR "$STARTMENU\Programs\VITCS"
!define START_LINK_PROGRAM "$STARTMENU\Programs\VITCS\VITCS.lnk"
!define START_LINK_UNINSTALLER "$STARTMENU\Programs\VITCS\Uninstall VITCS.lnk"
 
!define REG_UNINSTALL "Software\Microsoft\Windows\CurrentVersion\Uninstall\VITCS"
!define WEBSITE_LINK "http://77.120.161.224:81"

# Creating CleanUp function which will be called in Installer and Uninstaller
!macro CleanUp UN
Function ${UN}CleanUp
	# Shutdown application if it's running
	nsExec::Exec 'wmic process where name="Vitcs.Workstation.Client.exe" delete'

	# Remove Uninstaller registy entries
	DeleteRegKey HKCU "${REG_UNINSTALL}"
	DeleteRegValue HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\" "VITCS"

	# Delete Start Menu Shortcuts
	RMDir /r "${START_LINK_DIR}"

	# Delete Desktop shortcut
	Delete "$DESKTOP\VITCS.lnk"

	# Delete Taskbar icon
	${StdUtils.InvokeShellVerb} $0 "$INSTDIR" "Vitcs.Workstation.Client.exe" ${StdUtils.Const.ShellVerb.UnpinFromTaskbar}

	# Delete installation directory
	RMDir /r "$INSTDIR"
FunctionEnd
!macroend
!insertmacro CleanUp ""
!insertmacro CleanUp "un."

Function GetInstallationDirectoryFromCommandLineArgs
	# get command line args
	${GetParameters} $R0
	${GetOptions} $R0 "/InstallationDirectory=" $R1

	# if installation directory is not specified in command line then install to $LOCALAPPDATA\VITCS
	${If} $R1 == ""
		StrCpy $InstDir "$LOCALAPPDATA\VITCS"
	${Else}
		StrCpy $InstDir "$R1"
		Abort
	${EndIf}
FunctionEnd

# default section start
Section
	Call CleanUp

	# define output path
	SetOutPath $INSTDIR

	# specify file to go in output path
	File AlienRFID2.dll
	File Custom.PowerToolApi.dll
	File customxmltoimg.dll
	File customxmltoimg.dll.lib
	File libpng15.dll
	File Newtonsoft.Json.dll
	File QtCore4.dll
	File QtGui4.dll
	File QtXml4.dll
	File Vitcs.Workstation.Client.exe
	File Vitcs.Workstation.Client.exe.config
	File Vitcs.Workstation.Client.pdb
	File zint.dll
	File zlib125.dll

	SetOutPath $INSTDIR\PassPrintingTemplates\Step1

	File PassPrintingTemplates\Step1\PassPrinterStep1.param.xml
	File PassPrintingTemplates\Step1\PassPrinterStep1.xml

	SetOutPath $INSTDIR\PassPrintingTemplates\Step2

	File PassPrintingTemplates\Step2\PassPrinterStep2.param.xml
	File PassPrintingTemplates\Step2\PassPrinterStep2.xml

	SetOutPath $INSTDIR\ar

	File ar\Vitcs.Workstation.Client.resources.dll

	SetOutPath $INSTDIR\hi

	File hi\Vitcs.Workstation.Client.resources.dll

	SetOutPath $INSTDIR\ur

	File ur\Vitcs.Workstation.Client.resources.dll

	SetOutPath $INSTDIR

	# get cumulative size of all files in and under install dir
	# report the total in KB (decimal)
	# place the answer into $0  ($1 and $2 get other info we don't care about)
	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2

	# Convert the decimal KB value in $0 to DWORD
	# put it right back into $0
	IntFmt $0 "0x%08X" $0

	# Create/Write the reg key with the dword value
	WriteRegDWORD HKCU "${REG_UNINSTALL}" "EstimatedSize" "$0"

	WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayName" "VITCS"
	#WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayIcon" "$\"$INSTDIR\VITCS.ico$\""
	WriteRegStr HKCU "${REG_UNINSTALL}" "Publisher" "VITCS"

	WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayVersion" ${Version}
	WriteRegStr HKCU "${REG_UNINSTALL}" "HelpLink" "${WEBSITE_LINK}"
	WriteRegStr HKCU "${REG_UNINSTALL}" "URLInfoAbout" "${WEBSITE_LINK}"
	WriteRegStr HKCU "${REG_UNINSTALL}" "InstallLocation" "$\"$INSTDIR$\""
	WriteRegStr HKCU "${REG_UNINSTALL}" "InstallSource" "$\"$EXEDIR$\""
	WriteRegDWORD HKCU "${REG_UNINSTALL}" "NoModify" 1
	WriteRegDWORD HKCU "${REG_UNINSTALL}" "NoRepair" 1
	WriteRegStr HKCU "${REG_UNINSTALL}" "UninstallString" "$\"$INSTDIR\${UNINSTALLER_NAME}$\""
	WriteRegStr HKCU "${REG_UNINSTALL}" "Comments" "Uninstalls VITCS."

	# define uninstaller name
	WriteUninstaller $INSTDIR\${UNINSTALLER_NAME}

	# register windows startup item
	SetRegView 64
	WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\" "VITCS" '"$INSTDIR\Vitcs.Workstation.Client.exe" --daemon'

	# Register json stuff
	WriteRegStr HKCU "SOFTWARE\Classes\Mime\Database\Content Type\application/json\" "CLSID" "{25336920-03F9-11cf-8FD0-00AA00686F13}"

	SetShellVarContext current
	CreateDirectory "${START_LINK_DIR}"
	CreateShortCut "${START_LINK_PROGRAM}" "$INSTDIR\Vitcs.Workstation.Client.exe"
	CreateShortCut "${START_LINK_UNINSTALLER}" "$INSTDIR\${UNINSTALLER_NAME}"

	# Create Desktop shortcut
	CreateShortCut "$DESKTOP\VITCS.lnk" "$INSTDIR\Vitcs.Workstation.Client.exe" ""

	# Create Taskbar icon
	${StdUtils.InvokeShellVerb} $0 "$INSTDIR" "Vitcs.Workstation.Client.exe" ${StdUtils.Const.ShellVerb.PinToTaskbar}

	# Check if user has .NET Framework 4.7.2
    # Magic Number are from here https://docs.microsoft.com/en-us/dotnet/framework/migration-guide/how-to-determine-which-versions-are-installed
	ClearErrors
	ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Release"
	IfErrors dotNetFramework472NotDetected
	${If} $0 < 461808
	dotNetFramework472NotDetected:
		# go to .NET Framework 4.7.2 Web Installer download page
		MessageBox MB_OK "You need to install .NET Framework 4.7.2 to run application."

		ExecShell "open" "http://go.microsoft.com/fwlink/p/?LinkId=863265"

		Return
	${EndIf}

	Exec "$INSTDIR\Vitcs.Workstation.Client.exe"
SectionEnd
 
# create a section to define what the uninstaller does.
# the section will always be named "Uninstall"
Section "Uninstall"
	Call un.CleanUp
SectionEnd